import Keycloak from "keycloak-js";
import axios from "axios";
import {store} from "../store";

const kc = Keycloak('../assets/keycloak/keycloak.json');
const token = localStorage.getItem('kc_token');
const refreshToken = localStorage.getItem('kc_refreshToken');

kc.init({onLoad: 'login-required', token, refreshToken})
    .then(authenticated => {
        if (authenticated) {
            store.getState().keycloak = kc;
            updateLocalStorage();
           //TODO ReactDOM.render(app, document.getElementById("app"));
        }
    });

axios.interceptors.request.use(config => (
    kc.updateToken(5)
        .then(refreshed => {
            if (refreshed) {
                updateLocalStorage()
            }
            config.headers.Authorization = 'Bearer ' + kc.token;
            return Promise.resolve(config)
        })
        .catch(() => {
            kc.login();
        })
));

const updateLocalStorage = () => {
    localStorage.setItem('kc_token', kc.token);
    localStorage.setItem('kc_refreshToken', kc.refreshToken);
};