import '../node_modules/babel-polyfill';
import React from '../node_modules/react';
import ReactDOM from '../node_modules/react-dom';
import {Provider} from '../node_modules/react-redux';
import {store} from './store';
import App from './app/app';

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('app')
);
