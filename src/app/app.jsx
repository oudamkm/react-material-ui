import React from '../../node_modules/react';
import { Router, Route, Link } from '../../node_modules/react-router-dom';
import { history } from '../store';
import Main from './home/main';
import Home from './home/home';
import Profile from './secure/Profile';

export default class App extends React.Component {
    constructor(props){
        super();
    }

    render(){
        return(
            <Router onUpdate={() => window.scrollTo(0, 0)} history={history}>
                <div>
                    <Route exact path="/" component={Main} />
                    <Route path="/home" component={Home} />
                    <Route path="/secure" component={Profile} />
                </div>
            </Router>
        )
    }
}
