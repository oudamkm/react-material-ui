import React, { Component } from '../../../node_modules/react';
import { withStyles } from '@material-ui/core/styles';
import cover from '../../assets/img/pluralsight.jpg';

const styles = theme => ({
  indro: {
      background: '#222222',
      height: 500,
      marginLeft: -8
    }
});

class Indro extends Component {
    render() {
        const {classes} = this.props;
        return(
            <div className={classes.indro}>
                <img src={cover} height={500} width="100%" />
            </div>
        )
    }

}

export default withStyles(styles)(Indro);