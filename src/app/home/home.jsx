import React, { Component } from '../../../node_modules/react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '../../../node_modules/@material-ui/core/Grid';
import { FormControl, TextField, Button } from '../../../node_modules/@material-ui/core';
import HeadNav from './../component/navigator/head.nav';
import Indro from './indro';
import CourseCard from '../component/card/course.card';

const styles = theme => ({
    root: {
        flexGrow: 1,
        marginTop: 20
    },
    paper: {
        height: 140,
        width: 100,
    },
    control: {
        padding: theme.spacing.unit * 2,
    },
    freeSpace: {
        width: '100%',
        height: 78,
        background: '#000000',
        marginLeft: -8
    },
    container: {
        width: '100%',
        background: 'rgba(33,33,33,0.9)',
    }
});

class Home extends Component {
    constructor(){
        super();
        this.state = {
            name: ""
        }
    }

    getName() {

    }

    render() {
        const {classes} = this.props;
        return (
            <div className={classes.container}>
                <HeadNav />
                <div className={classes.freeSpace}>

                </div>
                <Indro />
                <Grid container className={classes.root} spacing={16}>
                    <Grid item xs={6}>
                        <Grid container justify="center" spacing={Number(16)}>
                            {[0, 1, 2].map(value => (
                                <Grid key={value} item>
                                    <CourseCard />
                                </Grid>
                            ))}
                        </Grid>
                    </Grid>
                </Grid>

                <div>
                    <h1 align="center">react-responsive-carousel</h1>
                </div>

                <Grid container spacing={16}>
                    <Grid item xs={12}>
                        <Grid container justify="center" spacing={Number(16)}>
                            <FormControl>
                                <TextField id="textName" name="textName" />
                                <Button type="submit" onClick={this.getName.bind(this)}> OK </Button>
                            </FormControl>
                        </Grid>
                    </Grid>
                </Grid>

                <Grid container spacing={16}>
                    <Grid item xs={12}>
                        <Grid container justify="center" spacing={Number(16)}>
                            {[0, 1, 2, 4].map(value => (
                                <Grid key={value} item>
                                    jjjjjjjjjj
                                </Grid>
                            ))}
                        </Grid>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default withStyles(styles)(Home);