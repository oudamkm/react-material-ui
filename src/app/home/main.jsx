import React, { Component } from '../../../node_modules/react/index';

export default class Main extends Component {
    render() {
        return (
            <div>
                { this.props.children }
            </div>
        );
    }
}
