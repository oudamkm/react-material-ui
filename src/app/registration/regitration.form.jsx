import React from 'react';
import { Row, Col, Image, Panel } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import { TextBox } from './../component/material-for-redux/custom.textfield';
import { ButtonSubmit } from './../component/material-for-redux/custom.button';

class RegistrationForm extends React.Component {
    constructor(props){
        super();
        this.state ={
            error : false
        };
    }

    handleSubmit(values){
        alert(JSON.stringify(values));

    }

    render(){
        const { handleSubmit, error, invalid, submitting } = this.props;
        return(
            <div>
                <br/><br/>
                <Row>
                    <Col xs={4} sm={4} md={4} lg={4}>
                        <center>
                            <Image src="/icons/bnd_copyright.png" responsive />
                            <br/>
                            <p style={{color:'blue'}}>Copyright © 2010 BnD Solutions Co., Ltd.</p>
                            <p style={{color:'blue'}}>All Rights Reserved.</p>
                        </center>
                        <br/><br/>
                        <Panel header={"Login Form"} bsStyle="primary">
                            <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                <Row className="wrap-row-form-login">
                                    <Col sm={12} xs={12} md={12} lg={12} className="login-page">
                                        <Field name="username" type="text" component={TextBox} label="Username"/>
                                    </Col>
                                </Row>
                                <Row className="wrap-row-form-login">
                                    <Col sm={12} xs={12} md={12} lg={12} className="login-page">
                                        <Field name="password" type="password" component={TextBox} label="Password"/>
                                    </Col>
                                </Row>
                                <Row className="wrap-row-form-login">
                                    <Col sm={4} xs={4} md={4} lg={4} className="login-page"></Col>
                                    <Col sm={4} xs={4} md={4} lg={4} className="login-page">
                                        <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label="Login" />
                                    </Col>
                                    <Col sm={4} xs={4} md={4} lg={4} className="login-page"></Col>
                                </Row>
                            </form>
                        </Panel>
                    </Col>
                </Row>
            </div>
        )
    }
}

RegistrationForm = reduxForm({
    form: 'form_sign_in',
    validate: (values) => {
        let regex_username = /[0-9a-zA-Z]{4,20}/;
        let regex_password = /[0-9a-zA-Z]{4,20}/;

        const errors = {};
        if (!regex_username.test(values.username) || values.username === undefined) {
            errors.username = 'Invalid username!';
        }
        if(!regex_password.test(values.password) || values.password === undefined){
            errors.password = 'Password is required at least 6 characters!';
        }
        return errors
    }
})(RegistrationForm);

export default RegistrationForm ;
