import React from '../../../../node_modules/react';
import {withStyles} from "@material-ui/core/styles/index";
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import CustomFormControl from './CustomFormControl';
import Grid from '../../../../node_modules/@material-ui/core/Grid';

const styles = theme => ({
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
        color: '#FFFFFF',
        padding: 0,
    },
    expand: {
        //display: 'flex',
        width: '100%',
        background: 'none!important',
        borderRadius: '0 !important',
        boxShadow: '0 0 0 #FFFFFF !important',
        borderBottom: '1px solid #FFFFFF',
        padding: 0,
    },
    expanded: {

    },
    summary: {
        '& $summaryExpanded': {
          height: 51,
          minHeight: '51px !important',
          background: '#F50057',
          //left: 0,
        },
        '& $summaryContent': {
            minHeight: '22px !important',
            height: 22,
        },
    },

    summaryExpanded: {
        // height: 51,
        // minHeight: '51px !important',
        // background: '#DEDEDE'
        '& $heading': {
            position: 'absolute',
            //right: 0
        }
    },
    summaryContent: {
        // minHeight: '22px !important',
        // height: 22,

    },
    expandDetail: {
        display: 'block',
        padding: 0
    },
    freeSpace: {
        height: 48,
    }
});


class CustomExpand extends React.Component {

    state = { clear: false, hidden: false };

    handleExpand = () => {
      this.setState(state => ({hidden: !state.hidden}))
    };

    render(){
        const { classes } = this.props;
        return(
            <Grid container>
                <Grid item xs={12}>
                    <ExpansionPanel expanded={this.state.hidden}  classes={{root: classes.expand, expanded: classes.expanded}} onChange={() => this.handleExpand()}>
                        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} classes={{root: classes.summary, expanded: classes.summaryExpanded, content: classes.summaryContent}}>
                            <Typography className={classes.heading}>SUPER CATEGORY</Typography>
                        </ExpansionPanelSummary>
                        <Grid container>
                            <Grid item xs={12} md={6}>
                                <ExpansionPanelDetails className={classes.expandDetail}>
                                    <CustomFormControl />
                                </ExpansionPanelDetails>
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <ExpansionPanelDetails className={classes.expandDetail}>
                                    <CustomFormControl />
                                </ExpansionPanelDetails>
                            </Grid>
                        </Grid>
                    </ExpansionPanel>
                </Grid>
                <Grid item xs={12}>
                    <div className={classes.freeSpace}>
                    </div>
                </Grid>
            </Grid>
        );
    }
}

export default withStyles(styles)(CustomExpand);