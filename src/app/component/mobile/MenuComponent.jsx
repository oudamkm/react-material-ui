import React from '../../../../node_modules/react';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import {withStyles} from "@material-ui/core/styles/index";
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '../../../../node_modules/@material-ui/icons/Menu';
import ClearIcon from '@material-ui/icons/Clear';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const styles = theme => ({
    root: {
        marginRight: 20,
        [theme.breakpoints.up('md')]: {
            display: 'none'
        },
    },
    gridLayout: {
        position: 'absolute',
        zIndex: 9999,
        flexGrow: 1,
        left: 0,
        width: '100%',
        maxHeight: 82,
        background: 'rgba(33,33,33,0.9)',
    },
    buttonSearch: {
        padding: '-20px',
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
        '&:active, &:focus, &:visited, &:hover': {
            borderBottom: '2px solid red'
        }
    },
    inputLayout: {
        //height: 82,
        paddingTop: 21,
    },
    clearButton: {
        float: 'right',
        top: -43,
        transition: 'opacity .3s ease',
        opacity: 0.1,
        '&:hover': {
            opacity: 0.5,
        }
    }
});


class MenuComponent extends React.Component {

    state = { clear: false, hidden: true };

    handleDrop = () => {
        this.setState(state => ({ hidden: !state.hidden }));
    };


    render(){
        const { classes } = this.props;
        return(
            <div className={classes.root}>
                <IconButton className={classes.buttonSearch} color="inherit" aria-label="Search" onClick={() => this.handleDrop()}>
                    {this.state.hidden ? <MenuIcon /> : <ClearIcon />}
                </IconButton>
                <ClickAwayListener onClickAway={() => this.setState({hidden: true})}>
                    <Grid hidden={this.state.hidden} className={classes.gridLayout}>
                        <Grid item xs={12}>
                            <div className={classes.inputLayout}>
                                <ExpansionPanel className={classes.expand}>
                                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                        <Typography className={classes.heading}>Expansion Panel 1</Typography>
                                    </ExpansionPanelSummary>
                                    <ExpansionPanelDetails>
                                        <Typography>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex,
                                            sit amet blandit leo lobortis eget.
                                        </Typography>
                                    </ExpansionPanelDetails>
                                </ExpansionPanel>
                                <ExpansionPanel className={classes.expand}>
                                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                        <Typography className={classes.heading}>Expansion Panel 2</Typography>
                                    </ExpansionPanelSummary>
                                    <ExpansionPanelDetails>
                                        <Typography>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex,
                                            sit amet blandit leo lobortis eget.
                                        </Typography>
                                    </ExpansionPanelDetails>
                                </ExpansionPanel>
                            </div>
                        </Grid>
                    </Grid>
                </ClickAwayListener>
            </div>
        );
    }
}

export default withStyles(styles)(MenuComponent);