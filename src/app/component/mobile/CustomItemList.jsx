import React from 'react';
import PropTypes from 'prop-types';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import {withStyles} from '@material-ui/core/styles';
import ListItemText from '@material-ui/core/ListItemText';

const styles = theme => ({
    root: {
       width: '100%'
    },
    menuList: {
        padding: 0,
        marginTop: -14,
    },
    menuItem: {
        width: '100%',
        height: 'auto',
        '&:focus': {
            borderBottom: '1px solid ' + theme.palette.primary.main,
            '& $primary, & $icon': {
                color: theme.palette.common.white,
            },
        },
        padding: '10px 0 10px 0',
    },
    primary: {
        all: 'unset',
        color: theme.palette.common.white,
        whiteSpace: 'normal !important',
        minHeight: 24,
        height: '100% !important'
    },
    icon: {},
});

class CustomItemList extends React.Component {

    render() {
        const {classes} = this.props;
        return (
            <div className={classes.root}>
                <MenuList className={classes.menuList}>
                    <MenuItem className={classes.menuItem}>
                        <ListItemText classes={{primary: classes.primary}} primary="Sent mail"/>
                    </MenuItem>
                    <MenuItem className={classes.menuItem}>
                        <ListItemText classes={{primary: classes.primary}} primary="Sent mail"/>
                    </MenuItem>
                    <MenuItem className={classes.menuItem}>
                        <ListItemText classes={{primary: classes.primary}} primary="Sent mail"/>
                    </MenuItem>
                </MenuList>
            </div>
        )
    }
}


CustomItemList.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CustomItemList);