import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormHelperText from '@material-ui/core/FormHelperText';
import CustomItemList from './CustomItemList';

const styles = theme => ({
    root: {
        display: 'flex',
    },
    formControl: {
        width: '100%',
        padding: theme.spacing.unit * 3,
    },
    heading: {
        width: '100%',
        fontSize: theme.typography.pxToRem(14),
        fontWeight: theme.typography.fontWeightRegular,
        color: '#FFFFFF',
        paddingBottom: 4,
        borderBottom: '2px solid #F50057',
    },
});

class CustomFormControl extends React.Component {
    render() {
        const { classes } = this.props;

        return (
            <div className={classes.root}>
                <FormControl component="fieldset" className={classes.formControl} fullWidth={true}>
                    <FormLabel className={classes.heading} component="legend">SUB CATEGORY</FormLabel>
                    <FormGroup>
                        <CustomItemList />
                    </FormGroup>
                    <FormHelperText>Be careful</FormHelperText>
                </FormControl>
            </div>
        );
    }
}

CustomFormControl.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CustomFormControl);