import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '../../../../node_modules/@material-ui/icons/Menu';
import Grid from '../../../../node_modules/@material-ui/core/Grid';
import Button from '../../../../node_modules/@material-ui/core/Button';
import CustomExpand from './CustomExpand';
import Loging from './Loging';
import CustomButton from '../Button/CustomButton';
import RegistrationPanel from './RegistrationPanel';

const styles = theme => ({
    paper: {
        display: 'flex',
        marginTop: 60,
        background: '#090909',
        padding: 0,
        borderRadius: '0 !important',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
        color: '#FFFFFF'
    },
    expand: {
        background: 'none !important',
        borderRadius: '0 !important',
        boxShadow: '0 0 0 #FFFFFF !important',
        borderBottom: '1px solid #FFFFFF'
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('md')]: {
            display: 'none'
        },
    },
});

class CategoryPanel extends React.Component {

    state = { right: false };

    toggleDrawer = (side, open) => () => {
        this.setState({
            [side]: open,
        });
    };

    render(){
        const { classes } = this.props;
        return(
            <div>
                <IconButton className={classes.menuButton} color="inherit" aria-label="Menu" onClick={this.toggleDrawer('right', true)}>
                    <MenuIcon />
                </IconButton>
                <SwipeableDrawer anchor="left" classes={{paper: classes.paper}} open={this.state.right} onOpen={this.toggleDrawer('right', true)} onClose={this.toggleDrawer('right', false)}>
                    <div
                        tabIndex={0}
                        role="button"
                    >
                        <div>
                            <RegistrationPanel />
                            <CustomExpand />
                        </div>
                    </div>
                </SwipeableDrawer>
            </div>
        )
    }
}

CategoryPanel.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CategoryPanel);