import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import CustomButton from '../Button/CustomButton';

const styles = theme => ({
    root: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
        borderRadius: 0,
        boxShadow: 0,
    },
});

class RegistrationPanel extends React.Component {
    render() {
        const { classes } = this.props;
        return(
            <Paper className={classes.root}>
                <CustomButton color="linear" round>
                    Try xxx for free
                </CustomButton>
                <Typography variant="headline" component="a">
                    Learn more about xxx ...
                </Typography>
            </Paper>
        )
    }
}


RegistrationPanel.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(RegistrationPanel);