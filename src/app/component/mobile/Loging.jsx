import React from 'react';
import createStyled from 'material-ui-render-props-styles';
import Button from '@material-ui/core/Button';

const styles = theme => ({
    root: {
            background: 'linear-gradient(4deg, #FE6B8B 30%, #e80a89 90%)',
            borderRadius: 3,
            border: 0,
            color: 'white',
            height: 48,
            padding: '0 30px',
            boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    },
});

// accepts same options as withStyles
const Styled = createStyled(styles);

const Loging = ({children}) => (
    <Styled>
        {({classes}) => (
            <Button className={classes.root}>
                {children}
            </Button>
        )}
    </Styled>
);

export default Loging;