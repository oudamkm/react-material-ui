import React from '../../../../node_modules/react';
import PropTypes from '../../../../node_modules/prop-types';
import { withStyles } from '../../../../node_modules/@material-ui/core/styles';
import Hidden from '@material-ui/core/Hidden';
import AppBar from '../../../../node_modules/@material-ui/core/AppBar';
import Toolbar from '../../../../node_modules/@material-ui/core/Toolbar';
import Typography from '../../../../node_modules/@material-ui/core/Typography';
import Button from '../../../../node_modules/@material-ui/core/Button';
import IconButton from '../../../../node_modules/@material-ui/core/IconButton';
import MenuIcon from '../../../../node_modules/@material-ui/icons/Menu';
import IntegrationAutosuggest from './search.text';
import NestedList from './list.nested';
import MobileSearch from '../search/MobileSearch';
import CategoryPanel from '../mobile/CategoryPanel';
import MenuComponent from '../mobile/MenuComponent';
import Categories from './Categories';

const styles = theme => ({
    appBar: {
        background: 'no-repeat',
    },
    toolbar: {
        background: 'rgba(33,33,33,0.9)',
        boxShadow: '0 10px 20px 0 rgba(0,0,0,.2)',
        [theme.breakpoints.up('md')]: {
            minHeight: '86px'
        },
        [theme.breakpoints.down('sm')]: {
            minHeight: '60px'
        },
        zIndex: 9999,
    },
    flex: {
        textAlign: 'center',
        marginRight: '20px',
        [theme.breakpoints.down('sm')]: {
            flex: 1
        },
    },
    menuButton: {
        //marginLeft: -12,
        marginRight: 20,
        [theme.breakpoints.up('md')]: {
            display: 'none'
        },
    },
    space: {
        flex: 1,
        [theme.breakpoints.down('sm')]: {
            flex: 0
        },
    },
    login: {
        float: 'right',
        [theme.breakpoints.down('sm')]: {
            display: 'none',
        },
    }
});

class HeadNav extends React.Component {
    state = { open: false };
    handleMobileMenu = () => {
        this.setState(state => ({open: true}))
    };

    render(){
        const { classes } = this.props;
        return (
            <div>
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar className={classes.toolbar}>
                        {/*<IconButton className={classes.menuButton} color="inherit" aria-label="Menu" onClick={() => this.handleMobileMenu()}>*/}
                            {/*<MenuIcon />*/}
                        {/*</IconButton>*/}
                        <CategoryPanel />
                        <Typography variant="title" color="inherit" className={classes.flex}>
                            Title
                        </Typography>
                        <Categories />
                        <NestedList />
                        <Hidden only={['sm', 'xs']}>
                            <IntegrationAutosuggest />
                        </Hidden>
                        <div className={classes.space}>
                            <Button color="inherit" className={classes.login}>Login</Button>
                            <MobileSearch />
                        </div>
                    </Toolbar>
                </AppBar>
            </div>
        )
    }
}

HeadNav.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(HeadNav);