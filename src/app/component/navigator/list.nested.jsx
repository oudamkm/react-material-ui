import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { NavLink } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Paper from '@material-ui/core/Paper';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import Grid from '@material-ui/core/Grid';
import KeyboardArrowDown from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUp from '@material-ui/icons/KeyboardArrowUp';
import SimpleCard from '../card/simple.card';

const styles = theme => ({
    cat: {
      marginRight: '20px',
        [theme.breakpoints.down('sm')]: {
            display: 'none',
        },
    },
    root: {
        position: 'absolute',
        zIndex: 500,
        //flexGrow: 1,
        marginTop: '21px',
        left: 0,
        width: '100%',
        background: '#2d3033',//'rgba(33,33,33,0.9)',
        padding: '26px 10%'
    },
    menuList: {
        borderRadius: 0,
        padding: 0,
        minHeight: 400,
    },
    menuItem: {
        '&:focus': {
            backgroundColor: theme.palette.primary.main,
            '& $primary, & $icon': {
                color: theme.palette.common.white,
            },
        },
    },
    primary: {},
    icon: {},
    button: {
        width: '120px',
        color: '#989898',
        backgroundColor: '#222222',
        borderRadius: '5px',
        border: '1px solid #555',
        fontWeight: '300',
        lineHeight: '24px',
        letterSpacing: 0,
        textTransform: 'uppercase',
        fontSize: '12px',
        textDecoration: 'none',
        transition: 'opacity .3s ease',
        [theme.breakpoints.down('xs')]: {
            display: 'none',
        },
        '&:active, &:focus, &:visited': {
            borderBottom: '2px solid red'
        }
    },
    paper: {
        background: '#2d3033',
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
        maxWidth: 260,
        maxHeight: 240
    },
});
class NestedList extends React.Component {
    state = { open: true, hidden: true };

    handleClick = () => {
        this.setState(state => ({ open: !state.open }));
    };

    handleDrop = () => {
        this.setState(state => ({ hidden: !state.hidden }));
    };

    handleMouseHover = () => {
        this.setState({ hidden: false });
    };

    handleMouseLeave = () => {
        this.setState({ hidden: true });
    };

    render() {
        const { classes } = this.props;

        return (
            <div className={classes.cat}>
                <Button
                    aria-label="delete"
                    className={classes.button}
                    onClick={this.handleDrop}
                    onMouseOver={this.handleMouseHover}
                >
                    Courses
                    {!this.state.hidden ? <KeyboardArrowUp className={classes.extendedIcon} /> : <KeyboardArrowDown className={classes.extendedIcon} />}
                </Button>
                <ClickAwayListener onClickAway={() => this.setState({hidden: true})}>
                    <div hidden={this.state.hidden} className={classes.root} >
                        <Grid container spacing={16}>
                            <Grid item md={2}>
                                <Paper className={classes.paper} square={true}>
                                    <SimpleCard />
                                </Paper>
                            </Grid>
                            <Grid item md={2}>
                                <Paper className={classes.paper} square={true}>
                                    <SimpleCard />
                                </Paper>
                            </Grid>
                            <Grid item md={2}>
                                <Paper className={classes.paper} square={true}>
                                    <SimpleCard />
                                </Paper>
                            </Grid>
                            <Grid item md={2}>
                                <Paper className={classes.paper} square={true}>
                                    <SimpleCard />
                                </Paper>
                            </Grid>
                        </Grid>

                        <Grid container spacing={16}>
                            <Grid item md={12}>
                                <NavLink
                                    to={"#"}
                                    className={classes.item}
                                    activeClassName="active"
                                >
                                    <ListItem button className={classes.itemLink}>
                                        <ListItemIcon className={classes.itemIcon}>
                                            <InboxIcon />
                                        </ListItemIcon>
                                        <ListItemText
                                            className={classes.itemText}
                                            disableTypography={true}
                                        />
                                    </ListItem>
                                </NavLink>
                            </Grid>
                        </Grid>
                    </div>
                </ClickAwayListener>

            </div>
        );
    }
}

NestedList.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NestedList);