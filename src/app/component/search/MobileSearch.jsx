import React from '../../../../node_modules/react';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import IntegrationAutosuggest from '../navigator/search.text';
import {withStyles} from "@material-ui/core/styles/index";
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import ToggleButton from '@material-ui/lab/ToggleButton';
import Search from '@material-ui/icons/Search';
import Clear from '@material-ui/icons/Clear';

const styles = theme => ({
    root: {
      float: 'right',
    },
    gridLayout: {
        position: 'absolute',
        zIndex: 9999,
        flexGrow: 1,
        left: 0,
        width: '100%',
        maxHeight: 82,
        background: 'rgba(33,33,33,0.9)',
    },
    inputLayout: {
        height: 82,
        padding: '21px 20px 0 20px',
    },
    clearButton: {
        float: 'right',
        top: -43,
        transition: 'opacity .3s ease',
        opacity: 0.1,
        '&:hover': {
            opacity: 0.5,
        }
    },
    toggleButton: {
        height: '47px !important',
        borderRadius: '40px !important',
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
        '&:focus, &:hover': {
            borderBottom: '2px solid red'
        }
    },

    selected: {
        borderBottom: '2px solid red',
    },
    label: {
        color: '#FFFFFF'
    }
});


class MobileSearch extends React.Component {

    state = { clear: false, hidden: true };

    handleDrop = () => {
        this.setState(state => ({ hidden: !state.hidden }));
    };

    handleCreateInput = () => {
        this.setState({clear: true});
    };

    render(){
        const { classes } = this.props;
        return(
            <div className={classes.root}>
                <ToggleButton value="left" selected={!this.state.hidden} classes={{root: classes.toggleButton, selected: classes.selected, label: classes.label}} onClick={() => this.handleDrop()}>
                    {this.state.hidden ? <Search /> : <Clear />}
                </ToggleButton>
                <ClickAwayListener onClickAway={() => this.setState({hidden: true})}>
                    <Grid hidden={this.state.hidden} className={classes.gridLayout}>
                        <Grid item xs={12}>
                           <div className={classes.inputLayout}>
                               <IntegrationAutosuggest clear={this.state.clear}/>
                               <IconButton className={classes.clearButton} color="inherit" aria-label="Clear" onClick={() => this.handleCreateInput()}>
                                   <Clear />
                               </IconButton>
                           </div>
                        </Grid>
                    </Grid>
                </ClickAwayListener>
            </div>
        );
    }
}

export default withStyles(styles)(MobileSearch);