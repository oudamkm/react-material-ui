import React, {PropTypes} from 'react';
import { FormGroup, FormControl, HelpBlock } from "react-bootstrap";

export const TextBox = ({ className, input, componentClass, type, label, children, disabled, icon="fa fa-keyboard-o", meta }) => (
    <FormGroup className={className} validationState={!meta.touched ? null : (meta.error ? 'error' : 'success')}>
        <FormControl {...input} componentClass={componentClass} type={type} disabled={disabled} placeholder={label} style={{paddingLeft: '30px', paddingRight: '5px', height: '34px'}}>
            {children}
        </FormControl>
        <FormControl.Feedback style={{left: 0, marginTop: '9px', width: '30px', height: '34px'}}>
            <i className={icon}>
            </i>
        </FormControl.Feedback>
        <HelpBlock>
            {meta.touched && meta.error ? meta.error : null}
        </HelpBlock>
    </FormGroup>
);
