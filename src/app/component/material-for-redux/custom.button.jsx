import React, {PropTypes} from 'react';
import { FormGroup, HelpBlock, Button } from "react-bootstrap";

export const ButtonSubmit = ({className, error, invalid, submitting, label, icon}) => (
    <div>
        { error && <FormGroup validationState="error"><HelpBlock>{error}</HelpBlock></FormGroup> }
        <FormGroup className="submit">
            <Button bsStyle="primary" type="submit" className={className} block disabled={invalid || submitting}>
                {label}&nbsp;&nbsp;<i className={icon}> </i>
            </Button>
        </FormGroup>
    </div>
);

