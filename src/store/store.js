import { createStore, applyMiddleware } from '../../node_modules/redux';
import thunkMiddleware from '../../node_modules/redux-thunk';
import rootReducer from '../reducer';

export const store = createStore(
    rootReducer,
    applyMiddleware(
        thunkMiddleware
    )
);