import { combineReducers } from '../../node_modules/redux';
import { reducer as formReducer } from '../../node_modules/redux-form';

const rootReducer = combineReducers({
    keycloak: (keycloak = {}) => keycloak,
    form: formReducer,
});

export default rootReducer;