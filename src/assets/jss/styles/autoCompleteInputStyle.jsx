const autoCompleteInputStyle = theme => ({
    container: {
        left: 0,
        flexGrow: 1,
        position: 'relative',
        height: 40,
        //width: '100%',
        //maxWidth: '500px'
    },
    suggestionsContainerOpen: {
        position: 'absolute',
        zIndex: 1,
        marginTop: theme.spacing.unit,
        left: 0,
        right: 0,
    },
    suggestion: {
        display: 'block',
    },
    suggestionsList: {
        margin: 0,
        padding: 0,
        listStyleType: 'none',
    },
    bootstrapInput: {
        color: '#FFFFFF',
        borderRadius: 4,
        backgroundColor: '#222222',
        border: '1px solid #555555',
        fontSize: 16,
        marginTop: '-1px',
        padding: '10.5px 38px',
        width: 'calc(100% - 24px)',
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        fontFamily: [
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
        ].join(','),
        '&:focus': {
            borderColor: '#80BDFF',
            boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
        },
    },
    bootstrapFormLabel: {
        fontSize: 18,
    },
    inputIcon: {
        position: 'absolute',
        display: 'flex',
        alignItems: 'center',
        zIndex: 500,
        margin: '10px 8px',
        color: '#FFFFFF'
    }
});

export default autoCompleteInputStyle;